/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.java;

import java.time.LocalDate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author KoonAoN
 */
public class jobServiceTest {
    
    public jobServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of checkEnbaleTime method, of class jobService.
     */
    @org.junit.jupiter.api.Test
    public void testCheckEnbaleTimeTodayIsBetweenStartTimeAndEndtime() {
        System.out.println("checkEnbaleTime");
        LocalDate startTime = LocalDate.of(2021,1,31);
        LocalDate endTime = LocalDate.of(2021,2,5);
        LocalDate todayTime = LocalDate.of(2021,2,3);
        boolean expResult = true;
        boolean result = jobService.checkEnbaleTime(startTime, endTime, todayTime);
        assertEquals(expResult, result);
    }
    public void testCheckEnbaleTimeTodayIsAfterEndTime() {
        System.out.println("checkEnbaleTime");
        LocalDate startTime = LocalDate.of(2021,1,31);
        LocalDate endTime = LocalDate.of(2021,2,5);
        LocalDate todayTime = LocalDate.of(2021,2,6);
        boolean expResult = false;
        boolean result = jobService.checkEnbaleTime(startTime, endTime, todayTime);
        assertEquals(expResult, result);
    }
    public void testCheckEnbaleTimeTodayIsBeforeStartTime() {
        System.out.println("checkEnbaleTime");
        LocalDate startTime = LocalDate.of(2021,1,31);
        LocalDate endTime = LocalDate.of(2021,2,5);
        LocalDate todayTime = LocalDate.of(2021,1,30);
        boolean expResult = false;
        boolean result = jobService.checkEnbaleTime(startTime, endTime, todayTime);
        assertEquals(expResult, result);
    }
    public void testCheckEnbaleTimeTodayIsEqualStartTime() {
        System.out.println("checkEnbaleTime");
        LocalDate startTime = LocalDate.of(2021,1,31);
        LocalDate endTime = LocalDate.of(2021,2,5);
        LocalDate todayTime = LocalDate.of(2021,1,31);
        boolean expResult = true;
        boolean result = jobService.checkEnbaleTime(startTime, endTime, todayTime);
        assertEquals(expResult, result);
    }
    public void testCheckEnbaleTimeTodayIsEqualEndTime() {
        System.out.println("checkEnbaleTime");
        LocalDate startTime = LocalDate.of(2021,1,31);
        LocalDate endTime = LocalDate.of(2021,2,5);
        LocalDate todayTime = LocalDate.of(2021,2,5);
        boolean expResult = true;
        boolean result = jobService.checkEnbaleTime(startTime, endTime, todayTime);
        assertEquals(expResult, result);
    }
}
