exports.checkEnableTime = function (startTime, endTime, todayTime) {
    if (todayTime < startTime) {
        return false
    }
    if (todayTime > endTime) {
        return false
    }
    if(todayTime == startTime){
        return true
    }
    if(todayTime == endTime){
        return true
    }
    if(todayTime < endTime && todayTime > startTime){
        return true
    }
}