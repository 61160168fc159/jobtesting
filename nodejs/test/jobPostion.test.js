const assert = require('assert')
const { describe } = require('mocha')
const { checkEnableTime } = require('../jobPostion')

describe('JobPostion', function () {
    it('shoud return false when เวลาสมัครอยู่ระหว่างเริ่มต้นและสิ้นสุด', function () {
        const startTime = new Date(2021, 1, 31)
        const endTime = new Date(2021, 2, 5)
        const todayTime = new Date(2021, 2, 3)
        const expectedResult = true

        const actualResult = checkEnableTime(startTime, endTime, todayTime)

        assert.strictEqual(actualResult, expectedResult)
    })
    it('shoud return false when เวลาสมัครเท่ากับเวลาเริ่มต้น', function () {
        const startTime = new Date(2021, 1, 31)
        const endTime = new Date(2021, 2, 5)
        const todayTime = new Date(2021, 1, 31)
        const expectedResult = true

        const actualResult = checkEnableTime(startTime, endTime, todayTime)

        assert.strictEqual(actualResult, expectedResult)
    })
    it('shoud return false when เวลาสมัครเท่ากับเวลาสิ้นสุด', function () {
        const startTime = new Date(2021, 1, 31)
        const endTime = new Date(2021, 2, 5)
        const todayTime = new Date(2021, 2, 5)
        const expectedResult = true

        const actualResult = checkEnableTime(startTime, endTime, todayTime)

        assert.strictEqual(actualResult, expectedResult)
    })
    it('shoud return false when เวลาสมัครอยู่ก่อนเวลาเริ่มต้น', function () {
        const startTime = new Date(2021, 1, 31)
        const endTime = new Date(2021, 2, 5)
        const todayTime = new Date(2021, 1, 30)
        const expectedResult = true

        const actualResult = checkEnableTime(startTime, endTime, todayTime)

        assert.strictEqual(actualResult, expectedResult)
    })
    it('shoud return false when เวลาสมัครอยู่หลังเวลาสิ้นสุด', function () {
        const startTime = new Date(2021, 1, 31)
        const endTime = new Date(2021, 2, 5)
        const todayTime = new Date(2021, 2, 6)
        const expectedResult = true

        const actualResult = checkEnableTime(startTime, endTime, todayTime)

        assert.strictEqual(actualResult, expectedResult)
    })
})
